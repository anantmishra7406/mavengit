package com.MavenProject.SpringBoot;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
public class JunitTesting {
	
	List<Integer> data = new ArrayList<Integer>(); 
	private Connection connection;
    private static Statement statement;
	private static ResultSet rs;
	public static  String url="jdbc:mysql://localhost:3306/test";
    public static  String user="root";
    public static  String password="root";    
    @SuppressWarnings("deprecation")
    public void setUp() {      
        connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Connecting to Database...");
            connection = DriverManager.getConnection(url, user, password);
            if (connection != null) {
                System.out.println("Connected to the Database...");
            }
        } catch (SQLException ex) {
           ex.printStackTrace();
        }
        catch (ClassNotFoundException ex) {
           ex.printStackTrace();
        }
    }
	public void userInsert( ) throws Exception  {
            try {            	            	 
                Class.forName("com.mysql.jdbc.Driver");
                Connection connection= DriverManager.getConnection(url,user,password);
                System.out.print("Connection is successful in database"+ url);                
                String query="Insert into student(sid,sname) value(3,'PCM')";               
               // PreparedStatement ps= connection.prepareStatement(query);                                                            
                Statement statement = connection.createStatement();
    			statement.execute(query);  
    			System.out.println("Insert data into Database...");
                //ps.executeUpdate();
                
            } catch (Exception e) {
                e.printStackTrace();
            }			 			  			 
       }
    
    public   List<Integer> userGetData() {
        try {        	
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection= DriverManager.getConnection(url,user,password);
            System.out.print("Connection is successful in database"+ url);                               
            String query1="SELECT * FROM student";
            Statement statement= connection.createStatement();
            ResultSet rs= statement.executeQuery(query1);
            while(rs.next())
            {
               int sid = rs.getInt("sid");
               String sname=rs.getString("sname");
               System.out.println( sid +"  "+sname + " ");
               data.add(sid);
            }          
            connection.close();
            System.out.println("INSERT student .. ");
        } catch (Exception e) {
            e.printStackTrace();
        }
		return data ;  
		 
   }
   	 		 	 
}
