package com.MavenProject.SpringBoot;
import java.awt.geom.RectangularShape;
import java.sql.*;
public class App 
{

    public static  String url="jdbc:mysql://localhost:3306/test";
    public static  String user="root";
    public static  String password="root";
    public static void userCreate( ) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection= DriverManager.getConnection(url,user,password);
            System.out.print("Connection is successful in database"+ url);
            Statement statement= connection.createStatement();         
            String query="SELECT * FROM student";
            ResultSet rs= statement.executeQuery(query);

            while(rs.next())
            {
               int id = rs.getInt("sid");
               String sname=rs.getString("sname");
               System.out.print( id +"  "+sname);
            }
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void userInsert( ) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection= DriverManager.getConnection(url,user,password);
            System.out.print("Connection is successful in database"+ url);
            String query="Insert into student(sid,sname) value(?,?)";
            PreparedStatement ps= connection.prepareStatement(query);
            ps.setString(1,"3");
            ps.setString(2,"DEMO");
            ps.executeUpdate();
            
            connection.close();
            System.out.print("INSERT student .. ");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void userUpdatedRec( ) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection= DriverManager.getConnection(url,user,password);

            String query="update student set sid=? WHERE  sname=?";
            PreparedStatement ps= connection.prepareStatement(query);
            ps.setInt(1,2);
            ps.setString(2,"DELHI");
            ps.executeUpdate();

            connection.close();
            System.out.print("UPDATED STUDENT RECORD..");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void userDeletedRec( ) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection= DriverManager.getConnection(url,user,password);

            String query="DELETE FROM student WHERE sid=?";
            PreparedStatement ps= connection.prepareStatement(query);
            ps.setInt(1,3);

            ps.executeUpdate();

            connection.close();
            System.out.print("DELETE STUDENT RECORD..");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void main(String[] args) {
        //userCreate();
         //userInsert();
        // userUpdatedRec();
         userDeletedRec();
    }
}
