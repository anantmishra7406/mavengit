package com.MavenProject.SpringBoot;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DbConnectMaven {
	
	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		DbConnectMaven demo = new DbConnectMaven();
		demo.setUp();
		demo.insertData();
}

   private Connection connection;
   private static Statement statement;
   private static ResultSet rs;
   String databaseURL ="jdbc:mysql://localhost:3306/test";
   String user = "root";
   String password = "root";
   List<Integer> data = new ArrayList<Integer>();
   public void setUp() {      
       connection = null;
       try {
           Class.forName("com.mysql.jdbc.Driver");
           System.out.println("Connecting to Database...");
           connection = DriverManager.getConnection(databaseURL, user, password);
           if (connection != null) {
               System.out.println("Connected to the Database...");
           }
       } catch (SQLException ex) {
          ex.printStackTrace();
       }
       catch (ClassNotFoundException ex) {
          ex.printStackTrace();
       }
   }

   public void insertData() throws SQLException, ClassNotFoundException {
	        Class.forName("com.mysql.jdbc.Driver");
            Connection connection= DriverManager.getConnection(databaseURL,user,password);
            System.out.print("Connection is successful in database"+ databaseURL);
	        String Query = "Insert into student(sid,sname)values(3,'PCM')";
			statement = connection.createStatement();
			statement.execute(Query);  
			System.out.println("Insert data into Database...");			  
   }

   
   public List<Integer> getData() throws ClassNotFoundException {
	   try {
		   Class.forName("com.mysql.jdbc.Driver");
           Connection connection= DriverManager.getConnection(databaseURL,user,password);
           System.out.print("Connection is successful in database"+ databaseURL);
           
            String query = "select * from student";
            statement = connection.createStatement();
            rs = statement.executeQuery(query);
            while(rs.next()){
                int EmpId= rs.getInt("sid");
                String EmpName= rs.getString("sname");             
                System.out.println(EmpId+"\t"+EmpName);
                data.add(EmpId);
            }
        } catch (SQLException ex) {
           ex.printStackTrace();
        }
    return data;
   }


}
