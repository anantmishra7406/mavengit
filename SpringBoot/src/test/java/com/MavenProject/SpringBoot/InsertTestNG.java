package com.MavenProject.SpringBoot;
import java.util.ArrayList;
import java.util.List;
import org.testng.annotations.Test;
public class InsertTestNG {
	List<Integer> data = new ArrayList<Integer>();
	JunitTesting dbdemo = new JunitTesting();
 	 
	@Test
	public void testForConnect() {
		dbdemo.setUp();
	}

	@Test
	public void testForInsertion() throws Exception {
		dbdemo.userInsert();
	}

	@Test
	public void testfordatafetch() throws ClassNotFoundException {
		data = dbdemo.userGetData();
		if(data.contains(2)) {
			System.out.println("Data is Present");
		}
	 }
}
