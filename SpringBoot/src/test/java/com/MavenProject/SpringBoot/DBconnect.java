package com.MavenProject.SpringBoot;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

public class DBconnect {
	List<Integer> data = new ArrayList<Integer>();

	DbConnectMaven dbdemo = new DbConnectMaven();
 
  @Test
  public void testForConnect() {
  	dbdemo.setUp();
  }

  @Test
  public void testForInsertion() throws SQLException, ClassNotFoundException {
  	dbdemo.insertData();
  }

  @Test
  public void testfordatafetch() throws ClassNotFoundException {
  	data = dbdemo.getData();
  	if(data.contains(2)) {
  		System.out.println("Data is Present");
  	}
   }
}
